<?php 
require_once './functions.php';
if (!isAuthorized()) {
  showError403();
}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <?php 
  if ((!empty($_GET))  && (array_key_exists('name', $_GET))) {
    $title = "Тест {$_GET['name']}";
  } else $title = "Тест не выбран";
   ?>
  <title><?=$title?></title>
</head>
<body>
<?php 
if ((!empty($_GET))  && (array_key_exists('name', $_GET))) {
  $testname =$_GET['name'];
  if (!file_exists("./tests/$testname")) {
    failTestName();
  }
  $file = file_get_contents("./tests/$testname");
  if (!$file) {    
    failTestName();
  }
  $array = json_decode($file, TRUE);
 ?>
 <form action="test.php" method="POST">
  <fieldset>
    <?php 
    $i = 0;
    foreach ($array as $value) {      
      shuffle($value['answers']); 
      echo "<legend>{$value['question']}</legend>";
      foreach ($value['answers'] as $questions) {
        echo '<label><input type="radio" value="'.$questions.'" required name="q'.$i.'"> '.$questions.'</label>';
      }
      $i++;
    }
    echo '<input type="hidden" value="'.$testname.'" name="numbertest">';
     ?>
  </fieldset>
  <input type="submit" value="Отправить">  
  </form>
  <a href="admin.php">Загрузить еще тесты</a><br>
  <a href="list.php">к списку тестов</a><br>
  <?php 

  } elseif (!empty($_POST)) {
  $testname = $_POST['numbertest'];
  $file = file_get_contents("./tests/$testname");
  $array = json_decode($file, TRUE);
  $i = 0;
  $username = $_SESSION['name'];
  echo "Привет, $username <br>";
  $true = 0;
  $question = 0;
  foreach ($_POST as $key => $value) {
    if ($key !== 'numbertest') {
      if ($value === $array[$i]['trueAnswer']) {
        echo "{$array[$i]['question']} <br> Ваш ответ: $value, Правильно! <br><br>"; 
        $true++;
        $question++;
      } else echo "{$array[$i]['question']} <br> Ваш ответ: $value, Неправильно! <br> Правильный ответ: {$array[$i]['trueAnswer']}<br><br>";
        $question++;
    }
    $i++;
  }
$result = ($true/$question)*2*100;

echo '<img src="./image.php?username='.$username.'&result='.$result.'" alt="sertificate"><br>';
?>
  <a href="admin.php">Загрузить еще тесты</a><br>
  <a href="list.php">к списку тестов</a><br>
<?php
echo '<a href="test.php?name='.$testname.'">Этот же тест заново</a>';
  }
else {
  failTestName();
}
?>


</body>
</html>