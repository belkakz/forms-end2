<?php 
	session_start();

	function showError403() {
  	  	http_response_code(403);
   		exit('<h1>403 Forbidden</h1><p>Перейти на <a href="index.php">главную</a></p>');
	}

	function showError404() {
  	  	http_response_code(404);
   		exit('<h1>403 Forbidden</h1><p>Перейти на <a href="index.php">главную</a></p>');
	}

	function logOut(){
		unset($_SESSION['name']);
		unset($_SESSION['role']);
		header('Location: ./index.php');
		exit;
	}

	function checkAuth($login, $pass) {
		$way = './users/'.$login.'.json';
		if (file_exists($way) ) {
			$loginDate = file_get_contents($way);
		    $logins = json_decode($loginDate, TRUE);
		    $logins = $logins[0];
	    	if ((isset($logins))&&($logins === $pass)) {
	    		$_SESSION['role'] = 'admin';
	        	return true;

	    	} 
	
		}	    

	    return false;
	}

	function isAuthorized($role = false) {
	    if (isset($_SESSION['role'])) {
	        if ($role === false or $_SESSION['role'] === $role) {
	            return true;
	        }
	    } 
	    
	    return false;
	}

  function failTestName() {
    header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
    echo "<h3>Тест не найден</h3>";
    echo '<a href="list.php">к списку тестов</a><br>';
    exit;
  }
 ?>