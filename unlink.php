<?php 
require_once './functions.php';
if ((isset($_SESSION['role'])) && $_SESSION['role'] === 'admin') {
	$filename = './tests/'.$_GET['name'];
	unlink($filename);
	$_SESSION['delete'] = $_GET['name'];
	header('Location: ./list.php');
}	else showError403();

 ?>