<?php 
require_once './functions.php';
if (!isset($_SESSION['name'])) {
	showError403();
	} 
else{	
	$username = $_SESSION['name'];
}
$folder = scandir('./tests/');
function testListPrint($folder) {
	foreach ($folder as $key => $value) {
		$testname = substr($value, 0, -5);
		if (($key !== 0) && ($key !== 1)) {
			echo '<a style="display: inline-block; margin-right:20px" href="./test.php?name='.$value.'">'.$testname.'</a> ';
			if ($_SESSION['role'] === 'guest') {
				echo '<br>';
			}
			if ($_SESSION['role'] === 'admin') {
				echo '<a style="color:red" href="./unlink.php?name='.$value.'">Удалить</a> <br>';
			}			
		}
	}
}

 
if (isset($_SESSION['delete'])) {
	echo "Файл удалён";
	unset($_SESSION['delete']);
}
	?>
	

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Список доступных тестов</title>
</head>
<body>
	<style>
		.upload {
			color: green;
			display: inline-block;
			margin-top: 10px;
		}
	</style>
<h3>Здравствуйте <?=$username?>!</h3>
<h4>Список доступных тестов:</h4>
<?php 
	testListPrint($folder);
 ?>
 <?php 
 	if ($_SESSION['role'] === 'admin') {
  		echo '<a class="upload" href="admin.php">Загрузить еще тесты</a><br>';
 	}
  ?> 
  <a href="index.php?out=1">выйти</a>
</body>
</html>