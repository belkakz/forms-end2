<?php 
require_once './functions.php';
if (isset($_SESSION['role'])) {
	header('Location: ./list.php');
}
if (!isset($_SESSION['try'])) {
	$_SESSION['try'] = 0;
}
	if ((!empty($_POST)) && ($_POST['name']) && (!empty($_POST['pass']))) {
		$_SESSION['name'] = $_POST['name'];
		if ($_POST['pass']) {
			$_SESSION['pass'] = $_POST['pass'];
			if (checkAuth($_SESSION['name'], $_SESSION['pass'])) {
				header('Location: ./list.php');
				$_SESSION['try'] = 0;
			}	else {
				$_SESSION['try']++;
				header('Location: ./index.php?who=admin');
			}
		}
	} 	elseif ((!empty($_POST)) && (!empty($_POST['name']))) {
			$_SESSION['name'] = $_POST['name'];
		    $_SESSION['role'] = 'guest';
			header('Location: ./list.php');
	}	elseif((!empty($_POST)) && (empty($_POST['name']))) {
			$_SESSION['name'] = 'Аноним';
	    	$_SESSION['role'] = 'guest';
		 	header('Location: ./list.php');
	}
 ?>

 <?php 
if ((isset($_GET['out'])) && ($_GET['out']==='1')) {
	logOut();
}
 if (!empty($_GET['who'])) {
 	if ($_GET['who'] === 'admin') {
 		function auth() {
 			$_SESSION['adm'] = 0;
 			if ($_SESSION['try']<1) {
 				echo '<h3>Здравствуйте администратор!</h3>';
 			} else 	echo '<h3>Верю что вы администратор, но все же пароль ввести потребуется правильно</h3>';
 			echo '<form action="index.php" method="POST">';
	 		echo '<label for="name">Введите ваше имя:</label>';
			echo '<input required type="text" name="name" id="name"><br><br>';
			echo '<label for="pass">Введите пароль: </label>';
			echo '<input required type="password" name="pass" id="pass">';
			echo '<input type="submit" name="" id=""><br>';
			echo '</form><br>';
 			echo '<a href="./index.php?who=guest">Войти как гость</a>';
 		}
 	}	elseif ($_GET['who'] === 'guest') { 
	 		function auth() {		
	 			unset($_SESSION['adm']);
	 			echo '<h3>Здравствуйте гость!</h3>';
	 			echo '<form action="index.php" method="POST">';
		 		echo '<label for="name">Введите ваше имя:</label>';
				echo '<input type="text" name="name" id="name">';
				echo '<input type="submit" name="" id="">';
				echo '</form>';
 				echo '<a href="./index.php?who=admin">Войти как администратор</a>';
	 		}
 		}
 }
  else {
 	function auth() {
 		echo '<h3>Здравствуйте гость!</h3>';
 		echo '<a href="./index.php?who=admin">Войти как администратор</a><br>';
 		echo '<a href="./index.php?who=guest">Войти как гость</a>';
 	}

 }
function tryAuth($tryNumb) {
	if ($_SESSION['try'] > 0) {
		echo '<strong>Вы ввели неверный пароль!!!</strong>';
		echo '<p>Попытка номер: '.$tryNumb.'</p>';
	}
};

 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Тесты</title>
</head>
<body>
	<?php 
		tryAuth($_SESSION['try']);
		auth();
		if ($_SESSION['try'] > 5) {
			echo '<img src="captcha.php" alt="">';
		
		}
	 ?>
  <br><a href="index.php?out=1">выйти</a>

	
</body>
</html>